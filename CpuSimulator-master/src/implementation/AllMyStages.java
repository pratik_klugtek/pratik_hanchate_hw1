/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation;

import implementation.AllMyLatches.*;
import tools.InstructionSequence;
import utilitytypes.EnumOpcode;
import baseclasses.InstructionBase;
import baseclasses.PipelineRegister;
import baseclasses.PipelineStageBase;
import voidtypes.VoidInstruction;
import voidtypes.VoidLatch;
import baseclasses.CpuCore;

/**
 * The AllMyStages class merely collects together all of the pipeline stage
 * classes into one place. You are free to split them out into top-level
 * classes.
 * 
 * Each inner class here implements the logic for a pipeline stage.
 * 
 * It is recommended that the compute methods be idempotent. This means that if
 * compute is called multiple times in a clock cycle, it should compute the same
 * output for the same input.
 * 
 * How might we make updating the program counter idempotent?
 * 
 * @author
 */
public class AllMyStages {
	/*** Fetch Stage ***/

	static class Fetch extends PipelineStageBase<VoidLatch, FetchToDecode> {

		public Fetch(CpuCore core, PipelineRegister input, PipelineRegister output) {
			super(core, input, output);
		}

		@Override
		public String getStatus() {
			// Generate a string that helps you debug.
			return null;
		}

		@Override
		public void compute(VoidLatch input, FetchToDecode output) {
			GlobalData globals = (GlobalData) core.getGlobalResources();
			int pc = globals.program_counter;
			// Fetch the instruction
			InstructionBase ins = globals.program.getInstructionAt(pc);
			InstructionBase ins_null = VoidInstruction.getVoidInstruction();
			

			if (ins.isNull())
				return;
			
			
			
			
			
			
			
		
			
			
			
			//xpSystem.out.println("-------------In Fetch instruction is " + ins);
			//xpSystem.out.println("PC" + globals.program_counter);
			// Do something idempotent to compute the next program counter.

			// Don't forget branches, which MUST be resolved in the Decode
			// stage. You will make use of global resources to commmunicate
			// between stages.

			// Your code goes here...

			// if(globals.register_invalid[ins.getOper0().getRegisterNumber()]==Boolean.FALSE)
			globals.program_counter++;

			
			
//			if(ins.getOpcode().toString().equals("CMP") && ins.getPCAddress()==25) {
//				
//				
//				if(ins.getSrc1().isRegister()) {
//					
//					int reg=ins.getSrc1().getRegisterNumber();
//					if(reg==globals.nextstagevariable) {
//						
//						//xpSystem.out.println("DEAMON IS here dm flag is Set true");
//						globals.deamonStall=true;
//						
//					}else {
//						globals.deamonStall=false;
//					}
//					
//					
//				}
//			}	
//			
//			
//			globals.instructiondependentonDecode=ins.getOper0().getRegisterNumber();
//          
//			
//			
			
			
			
			output.setInstruction(ins);
			
//			  if(ins.getPCAddress()==25 && globals.deamonStall==true) {
//					
//				  //xpSystem.out.println("DEAMON is here");
//				//	globals.program_counter=ins.getPCAddress();
//				//	output.setInstruction(ins_null);
//				}
			

				
			  
		}

		@Override
		public boolean stageWaitingOnResource() {
			// Hint: You will need to implement this for when branches
			// are being resolved.
			return false;
		}

		/**
		 * This function is to advance state to the next clock cycle and can be applied
		 * to any data that must be updated but which is not stored in a pipeline
		 * register.
		 */
		@Override
		public void advanceClock() {
			// Hint: You will need to implement this help with waiting
			// for branch resolution and updating the program counter.
			// Don't forget to check for stall conditions, such as when
			// nextStageCanAcceptWork() returns false.
			//xpSystem.out.println("in Fetch advanceClock");

		}

	}

	/*** Decode Stage ***/
	static class Decode extends PipelineStageBase<FetchToDecode, DecodeToExecute> {
		public Decode(CpuCore core, PipelineRegister input, PipelineRegister output) {
			super(core, input, output);
		}

		@Override
		public boolean stageWaitingOnResource() {
			// Hint: You will need to implement this to deal with
			// dependencies.
			return false;
		}

		@Override
		public void compute(FetchToDecode input, DecodeToExecute output) {
			InstructionBase ins = input.getInstruction();

			//xpSystem.out.println("----------in Decode ins is" + ins);

			// These null instruction checks are mostly just to speed up
			// the simulation. The Void types were created so that null
			// checks can be almost completely avoided.

			 InstructionBase ins_null = VoidInstruction.getVoidInstruction();
				
			if (ins.isNull())
				return;

			
			
			GlobalData globals = (GlobalData) core.getGlobalResources();
			
			
			
			//xpSystem.out.println("PC" + globals.program_counter);
			int[] regfile = globals.register_file;

			int ipos = ins.getInstructionString().indexOf(";");

			int tx=ins.getOper0().getRegisterNumber();
			int bx=ins.getSrc1().getRegisterNumber();
			
			try {
			
			// Delete the comments in the instruction....
			String s = ins.getInstructionString().substring(0, ipos);

		
			
				
			
			
			String[] instarray = s.split("\\s+");

			for (int i = 0; i < instarray.length; i++) {

				instarray[i] = instarray[i].replaceAll("[^\\w]", "");
				// //xpSystem.out.println("ELEMENT at :"+i+ "is"+instarray[i] );
			}

			String st = ins.getOpcode().toString().toLowerCase();

			// //xpSystem.out.println("INS is :"+ins);

			String keym;
			int valm = 0;

			
			
			
			
			
			
			switch (st) {

			case "movc":
				//xpSystem.out.println("type is movc");

				keym = instarray[2];
				valm = Integer.parseInt(instarray[3]);
				

				globals.hm.put(keym, valm);
				ins.getSrc1().setValue(valm);

				//xpSystem.out.println("REG number:" + ins.getOper0().getRegisterNumber());

				break;

			case "add":
			case "cmp":
			case "store":
			case "load":
				//xpSystem.out.println("type is add");
				// ** ins.getSrc1().setValue(v);

				int rpos = ins.getSrc1().getRegisterNumber();
				int tempval = globals.register_file[rpos];

				//xpSystem.out.println("rpos" + rpos + "tempval" + tempval);

				
				
				//deamon stall
				
				if(ins.getOpcode().toString().toLowerCase().equals("load")&& ins.getPCAddress()==24){
					
					//xpSystem.out.println("DEAMON is here variable set"+ins.getOper0().getRegisterNumber());
					
					globals.nextstagevariable=ins.getOper0().getRegisterNumber();
					
				}
				
				
				
			

				
				
			
				
				if (ins.getSrc2().isRegister()) {
				//	globals.register_invalid[ins.getSrc2().getRegisterNumber()] = true;
					//xpSystem.out.println("add with register");
					output.source2value = globals.register_file[ins.getSrc2().getRegisterNumber()];
					ins.getSrc2().setValue(output.source2value);
					//xpSystem.out.println("source2value" + output.source2value);
				}
				

				if (ins.getSrc2().hasValue()) {

					//xpSystem.out.println("add with value" + ins.getSrc2().getValue());
					output.source2value = ins.getSrc2().getValue();

				}

				output.destination_index = ins.getOper0().getRegisterNumber();
				output.register_bucket = tempval;
				ins.getSrc1().setValue(tempval);
				//xpSystem.out.println("xxxx" + tempval);

				break;

			case "HALT":

				//xpSystem.out.println("type is halt");
				globals.runnin=false;

				break;

			case "bra":
				//xpSystem.out.println("type is  branch");

				globals.branch_flag = true;
				

				//xpSystem.out.println("br Opcode :" + ins.getOpcode());
				//xpSystem.out.println("br oper0 :" + globals.register_file[2]);
				//xpSystem.out.println("br src1 :" + ins.getSrc1().getRegisterNumber());
				//xpSystem.out.println("br src1value :" + ins.getSrc1().getValue());
				//xpSystem.out.println("br src2 :" + ins.getSrc2().getRegisterNumber());
				//xpSystem.out.println("br src2Val :" + ins.getSrc2().getValue());

				int regindex = ins.getOper0().getRegisterNumber();
				int operval = globals.register_file[regindex];
				//xpSystem.out.println("operval :"+operval);

				if (ins.getInstructionString().contains("lt")) {

					
					
					//xpSystem.out.println("label target :" + ins.getLabelTarget().getAddress());
					//xpSystem.out.println("r1 value" + globals.register_file[1]);

					//xpSystem.out.println("labeltarget"+ins.getLabelTarget().getAddress());
					
					//xpSystem.out.println("register invalid :"+globals.register_invalid[tx]+tx);
					
					
					
					
					if(operval==-1 ) {
						
					
					if(globals.register_invalid[tx]==false) {	
					globals.program_counter = ins.getLabelTarget().getAddress() ;
					}
				//	globals.gbranch=true;
					ins.getOper0().setValue(operval);
					output.opervalb = operval;
					}
					//	globals.program_counter=ins.getPCAddress();
				

				}

				if (ins.getInstructionString().contains("eq") ) {

					if(operval==4) {
				
					if(globals.register_invalid[tx]==false)	
					globals.program_counter = ins.getLabelTarget().getAddress() ;
					
					//globals.gbranch=true;
					ins.getOper0().setValue(operval);
					output.opervalb = operval;
					}
				

				}

				if (ins.getInstructionString().contains("ge") ) {

					if(operval==1||operval==4) {
					
						if(globals.register_invalid[tx]==false)	
					globals.program_counter = ins.getLabelTarget().getAddress();
				
				//	globals.gbranch=true;
					ins.getOper0().setValue(operval);
					output.opervalb = operval;
					}
				}
				

				break;

			case "jmp":
				//xpSystem.out.println("type is jmp");
				globals.program_counter = ins.getLabelTarget().getAddress();
				//xpSystem.out.println("jump target :" + ins.getLabelTarget().getAddress());
			//	globals.branch_flag=true;
             //**   output.setInstruction(ins_null);
				
				break;

			case "out":
				
				
				break;
				
			case "halt":	
				//xpSystem.out.println("HALT");
				
				break;
				
			default:
				throw new IllegalArgumentException("Invalid Exception Type");

			}
			
			} catch (Exception e) {
				// TODO: handle exception
			}

			// Do what the decode stage does:
			// - Look up source operands
			// - Decode instruction
			// - Resolve branches

			
			
			
				
			

			//xpSystem.out.println("check1" +tx );
			
		try {
			
			
			
			
			if (globals.register_invalid[tx] == false) {
				//xpSystem.out.println("deamon");
				output.setInstruction(ins);
				globals.pc_negate=false;
				
//				
//				if(!globals.deamonStall) {
//					//xpSystem.out.println("notdmstall");
//					output.setInstruction(ins);
//				}else {
//					//xpSystem.out.println("dmstall");
//					//output.setInstruction(ins_null);
//				}

			} else {
				//xpSystem.out.println("pc has been negated ##");
				globals.program_counter--;
				
				
				if(ins.getPCAddress()==29) {
					//globals.program_counter++;
					output.setInstruction(ins);
					
				}
				
					
				
				globals.pc_negate=true;
			}
				
		} catch (Exception e) {
			// TODO: handle exception
		}
				
			
				
			
			
			
	
			// Set other data that's passed to the next stage.
			


			try {
				globals.register_invalid[ins.getOper0().getRegisterNumber()] = true;
				//xpSystem.out.println("ggg"+ins.getOper0().getRegisterNumber());			
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		}
		
		

	}

	/*** Execute Stage ***/
	static class Execute extends PipelineStageBase<DecodeToExecute, ExecuteToMemory> {
		public Execute(CpuCore core, PipelineRegister input, PipelineRegister output) {
			super(core, input, output);
		}

		String TAG = "Execute";

		@Override
		public void compute(DecodeToExecute input, ExecuteToMemory output) {
			
			
			try {
				
			//	//xpSystem.out.println("3");
			
			InstructionBase ins = input.getInstruction();
			GlobalData globals = (GlobalData) core.getGlobalResources();
			//xpSystem.out.println("---------------In Execute ins is :" + ins);
			//xpSystem.out.println("PC" + globals.program_counter);


			

			int source1 = ins.getSrc1().getValue();
			int source2 = ins.getSrc2().getValue();
			int oper0 = ins.getOper0().getValue();

			//xpSystem.out.println("src2val :" + input.source2value);

			//xpSystem.out.println("Source1 :" + source1 + "Source2 :" + source2 + "oper0 :" + oper0);

			
			
			
			int result = MyALU.execute(ins.getOpcode(), source1, source2, oper0);

			//xpSystem.out.println("Result is :" + result);

			if (ins.getOpcode().toString().toLowerCase().equals("store")) {

				output.offsetvalue = result;
			}

			output.result = result;
			
			//xpSystem.out.println("deamonE"+ins.getOper0().getRegisterNumber());

		
			
		
			
			
			
			
			// Fill output with what passes to Memory stage...

			// if(!globals.register_invalid[ins.getOper0().getRegisterNumber()])
			output.setInstruction(ins);

			} catch (Exception e) {
				// TODO: handle exception
			}
			// Set other data that's passed to the next stage.
		}
	}

	/*** Memory Stage ***/
	static class Memory extends PipelineStageBase<ExecuteToMemory, MemoryToWriteback> {
		public Memory(CpuCore core, PipelineRegister input, PipelineRegister output) {
			super(core, input, output);
		}

		String TAG = "Memory";

		@Override
		public void compute(ExecuteToMemory input, MemoryToWriteback output) {
			
			try {
				
			
			InstructionBase ins = input.getInstruction();

			//xpSystem.out.println("-------------in Memory ins is " + ins);

			GlobalData globals = (GlobalData) core.getGlobalResources();
			//xpSystem.out.println("PC" + globals.program_counter);

			if (ins.isNull())
				return;

			if (ins.getOpcode().toString().toLowerCase().equals("load")) {

				//xpSystem.out.println("Instruction is LOad  ");
				// store into mem
				int regpos = ins.getOper0().getRegisterNumber();
				
				int mempos=globals.register_file[ins.getSrc1().getRegisterNumber()];
				
				
				
				//xpSystem.out.println("mempos is "+mempos);
				
				output.reg_position=regpos;
				output.mem_position=mempos;
				
				
				
				

			}

			if (ins.getOpcode().toString().toLowerCase().equals("store")) {

				//xpSystem.out.println("Instruction is store ");
				int mempos = 0;
				int regpos = 0;

				if (input.offsetvalue != 0) {

					int s1=globals.register_file[ins.getSrc1().getRegisterNumber()];
					regpos = s1+input.offsetvalue;
					mempos = globals.register_file[ins.getOper0().getRegisterNumber()];
					//xpSystem.out.println("STORE with offset :" + mempos + "previos reg val =" + globals.register_file[1]);
				} else {
					
					//xpSystem.out.println(ins.getOper0().getRegisterNumber()+"**"+ins.getSrc1().getRegisterNumber());
					
					mempos =   globals.register_file[ins.getOper0().getRegisterNumber()]; 
					regpos = globals.register_file[ins.getSrc1().getRegisterNumber()];
					
					
				}
				
				//xpSystem.out.println("MM"+mempos+regpos);
				
				
				globals.memory[regpos] = mempos;
				

				// for (int i=0;i<globals.memory.length;i++)
				// //xpSystem.out.println(globals.memory[i]);
				
				for (int i=0; i<100;i++) {
					
					//xpSystem.out.println("m"+globals.memory[i]);
				}

			} else

				output.result = input.result;

			//xpSystem.out.println("Access memory :" + ins.getSrc1().getValue());

			output.setInstruction(ins);
			// Set other data that's passed to the next stage.
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			}
	}

	/*** Writeback Stage ***/
	static class Writeback extends PipelineStageBase<MemoryToWriteback, VoidLatch> {
		public Writeback(CpuCore core, PipelineRegister input, PipelineRegister output) {
			super(core, input, output);
		}

		@Override
		public void compute(MemoryToWriteback input, VoidLatch output) {
			InstructionBase ins = input.getInstruction();

			//xpSystem.out.println("---------------In WB ins is" + ins);

			if (ins.isNull())
				return;

			// Write back result to register file
			GlobalData globals = (GlobalData) core.getGlobalResources();

			//xpSystem.out.println("PC" + globals.program_counter);

			int regnumber = ins.getOper0().getRegisterNumber();

			int resultval = input.result;

			//xpSystem.out.println("REGnumber :" + regnumber + "FINAL WB VALUE:" + resultval);

			// writeback in register

			// if(!ins.getOpcode().toString().toLowerCase().equals("bra")&&!ins.getOpcode().toString().toLowerCase().equals("store"))

			if (EnumOpcode.needsWriteback(ins.getOpcode())) {

				if(ins.getOpcode().toString().equals("LOAD")) {
					globals.register_file[input.reg_position] = globals.memory[input.mem_position];
					
					//******globals.register_invalid[ins.getSrc1().getRegisterNumber()] = false;
					//xpSystem.out.println("Loadjson ins is " +globals.memory[input.mem_position]);
					
					if(ins.getPCAddress()==24) {
						globals.deamonStall=false;
						globals.nextstagevariable=0;
						
					}
					
				
					
				}else {
				
				//xpSystem.out.println("needs wb");
				globals.register_file[regnumber] = resultval;
				}
				
				

			} else {
				//xpSystem.out.println("doesnt need wb");
			}

			int tx=ins.getOper0().getRegisterNumber();
			
			globals.register_invalid[tx] = false;
			
			if(ins.getPCAddress()==26 &&tx==globals.instructiondependentonDecode) {
				//xpSystem.out.println("XXXX");
			globals.register_invalid[ins.getOper0().getRegisterNumber()] = true;
			}
			
			
			

			for (int i = 0; i < globals.register_file.length; i++) {

				//xpSystem.out.println(globals.register_file[i]);

			}
			
			for(int i=0;i<100;i++) {
				//xpSystem.out.println("m"+globals.memory[i]);
			}
			
			if(globals.branch_flag==true) {
				globals.branch_flag=false;
			}
			
			if(ins.getOpcode().toString().equals("OUT")) {
				int op=globals.memory[globals.register_file[2]];
			
				if(op!=0)
				System.out.println("OUTPUT@@:"+op );
			
			globals.finalresult[globals.fnr]=op;
			globals.fnr++;
			}
			
			for(int i=0;i<100;i++) {
				
				//xpSystem.out.println("TT"+globals.finalresult[i]);
				
			}
			
			if (input.getInstruction().getOpcode() == EnumOpcode.HALT) {
				// Stop the simulation
				globals.runnin=false;
				//globals.program_counter=-1;
			}
		}
	}
}
