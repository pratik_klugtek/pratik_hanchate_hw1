/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package implementation;

import utilitytypes.EnumOpcode;

/**
 * The code that implements the ALU has been separates out into a static
 * method in its own class.  However, this is just a design choice, and you
 * are not required to do this.
 * 
 * @author 
 */
public class MyALU {
    static int execute(EnumOpcode opcode, int input1, int input2, int oper0) {
        int result = 0;
        String TAG="ALU--->";
        
        // Implement code here that performs appropriate computations for
        // any instruction that requires an ALU operation.  See
        // EnumOpcode.
        GlobalData globals=new GlobalData();
        
        
        if(opcode.toString().equals("MOVC")){
			result= input1;
        	}else if(opcode.toString().equals("ADD")) {
		
        		result= input1+input2;
        		
		}else if(opcode.toString().equals("CMP")) {
			
			//xpSystem.out.println("ALU :CMP");
			//xpSystem.out.println("Input1## :"+input1);
			//xpSystem.out.println("Input2## :"+input2);
			
			
			
			if(input1<input2) {
				result=-1;
			}
			else if (input1>input2) {
				result=1;
			}else if(input1==input2) {
				result=4;
			}
		}else if(opcode.toString().equals("STORE")) {
			//put value in memory..
			//xpSystem.out.println("ALU:Store");
			
			
			
			if(input2!=0) {
				
			result=input1+input2;	
				
			}
			
			
		}else if(opcode.toString().equals("BRA")) {
			
			//xpSystem.out.println(TAG+"BRAAAAAAA"+opcode.name()+"ip1"+input1+"ip2"+input2+"oper0 :"+oper0);
			
			
			
			
			
		}else if(opcode.toString().equals("OUT")) {
		
			//xpSystem.out.println(TAG+"OUTPUTXX"+opcode.name()+"ip1"+input1+"ip2"+input2+"oper0 :"+oper0);
		}
        
        else if(opcode.toString().equals("HALT")) {
			
		}     
        
        return result;
    }    
}
